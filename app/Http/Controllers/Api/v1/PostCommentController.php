<?php

namespace App\Http\Controllers\Api\v1;

use App\Entities\Post;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\v1\CommentRequest;
use App\Http\Resources\Comment as CommentResource;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PostCommentController extends Controller
{
    /**
     * Return the post's comments.
     */
    public function index(Request $request, Int $post_id): JsonResponse
    {
        $data = [];
        $comments = Post::find($post_id)->comments()->with('author')->get();
        if ($comments) {
            foreach ($comments as $comment) {
                $data[] = [
                    'id' => $comment->id,
                    'parent_id' => $comment->parent_id,
                    'author' => $comment->author->name,
                    'author_id' => $comment->author_id,
                    'text' => $comment->text,
                    'date' => $comment->created_at->format('d/m/Y')
                ];
            }
        }

        return response()->json([
            'data' => $data
        ], 200);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(CommentRequest $request, Post $post) :CommentResource
    {
        return new CommentResource(
            Auth::user()->comments()->create($request->only(['parent_id', 'post_id', 'text']))
        );
    }
}
