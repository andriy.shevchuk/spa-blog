<?php

namespace App\Http\Controllers\Api\v1;

use App\Entities\Post;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\v1\PostRequest;
use App\Http\Resources\Post as PostResource;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PostController extends Controller
{
    /**
     * Return all posts.
     */
    public function index(): JsonResponse
    {
        $data = [];
        $posts = Post::with('author', 'media')->get();

        if ($posts) {
            foreach ($posts as $key => $post) {
                $imageUrl = null;
                $imageBase64 = null;
                if ($post->media) {
                    foreach ($post->media as $id => $media) {
                        $imageBase64[$id] = base64_encode($media->contents());
                        $imageUrl[$id] = $media->getUrl();
                    }
                }

                $data[$key] = [
                    'id' => $post->id,
                    'title' => $post->title,
                    'imageSrc' => $imageUrl,
                    'img_base64' => $imageBase64,
                    'author' => $post->author->name,
                    'author_id' => $post->author_id,
                    'teaser' => $post->teaser,
                    'date' => $post->created_at->format('d/m/Y')
                ];
            }
        }

        return response()->json([
            'data' => $data
        ], 200);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(PostRequest $request, Post $post): PostResource
    {

        $post->update($request->only(['title', 'content', 'teaser']));

        return new PostResource($post);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(PostRequest $request): PostResource
    {
        $media_array = $request->only('image');
        $media_ids = [];

        foreach ($media_array['image'] as $media) {
            $media_ids[] = array_get($media, 'id');
        }

        $post = Auth::user()->posts()->create($request->only(['title', 'content', 'teaser']));
        if ($media_ids) {
            $post->attachMedia($media_ids, 'original');
        }

        return new PostResource(
            $post
        );
    }

    /**
     * Return the specified resource.
     */
    public function get(Int $id): PostResource
    {
        $post = Post::where('id', $id)->first();
        if ($post) {
            return new PostResource($post);
        }

        return response()->json([
            'message' => 'Record not found',
        ], 404);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Request $request): JsonResponse
    {
        $id = $request->get('post_id');
        $post = Post::where('id', $id)->first();
        $post->delete();

        return response()->json([
            'message' => 'Remove post',
        ], 200);
    }
}
