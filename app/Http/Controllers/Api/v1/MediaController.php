<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use MediaUploader;

class MediaController extends Controller
{

    public function upload(Request $request): JsonResponse
    {

        $media = MediaUploader::fromSource($request->file('file'))
            ->toDirectory('/public/post_images')
            ->upload();

        return response()->json([
            'data' => $media,
        ], 200);
    }}
