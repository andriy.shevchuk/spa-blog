<?php

namespace App\Http\Requests\Api\v1;

use Carbon\Carbon;
use Illuminate\Foundation\Http\FormRequest;

class PostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'title' => 'required',
            'teaser' => 'required',
            'content' => 'required',
            'image.id' => 'nullable|exists:media,id',
        ];
    }
}
