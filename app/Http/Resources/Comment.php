<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Comment extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'parent_id' => $this->parent_id,
            'text' => $this->text,
            'author_id' => $this->author_id,
            'author' => $this->author->name,
            'date' => $this->created_at->format('d/m/Y')
        ];
    }
}
