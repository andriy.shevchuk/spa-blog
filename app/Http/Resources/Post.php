<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class Post extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $imageUrl = null;
        $imageBase64 = null;

        if ($this->media) {
            foreach ($this->media as $media) {
                $imageBase64[] = base64_encode($media->contents());
                $imageUrl[] = $media->getUrl();
            }
        }

        return [
            'id' => $this->id,
            'title' => $this->title,
            'teaser' => $this->teaser,
            'content' => $this->content,
            'imageSrc' => $imageUrl,
            'img_base64' => $imageBase64,
            'author_id' => $this->author_id,
            'author' => $this->author->name,
            'date' => $this->created_at->format('d/m/Y')
        ];
    }
}
