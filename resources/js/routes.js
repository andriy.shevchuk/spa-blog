import Login from './components/auth/Login.vue';
import Register from './components/auth/Register.vue';
import NewPost from './components/blog/PostForm.vue';
import Post from './components/blog/Post.vue';
import Home from './components/blog/ListPosts.vue';

export const routes = [
    {
        path: '/',
        component: Home,
        meta: {
            title: 'Home Page - Example App',
        }
    },
    {
        path: '/login',
        component: Login
    },
    {
        path: '/register',
        component: Register
    },
    {
        path: '/create/post',
        component: NewPost,
        meta: {
            requiresAuth: true
        }
    },
        {
            path: '/post/:id',
            component: Post,

    }
];
