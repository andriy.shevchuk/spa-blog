import {getLocalUser} from "./helpers/auth";

const user = getLocalUser();

export default {
    state: {
        currentUser: user,
        isLoggedIn: !!user,
        loading: false,
        auth_error: null,
        posts: [],
        comments: [],
        countComments: 0,
        postId: 0,
    },
    getters: {
        isLoading(state) {
            return state.loading;
        },
        isLoggedIn(state) {
            return state.isLoggedIn;
        },
        currentUser(state) {
            return state.currentUser;
        },
        currentPostId(state) {
            return state.post_id;
        },
        authError(state) {
            return state.auth_error;
        },
        posts(state) {
            return state.posts;
        },
        getComments: function (state) {

            return state.comments.filter((item) => {
                return item.parent_id === 0;
            });
        },
        getChildren: (state) => (id) => {

            return state.comments.filter((item) => {
                return item.parent_id === parseInt(id);
            });
        },
        getCountComments: (state) => {
            return state.comments.length;
        },
    },
    mutations: {
        login(state) {
            state.loading = true;
            state.auth_error = null;
        },
        loginSuccess(state, payload) {
            state.auth_error = null;
            state.isLoggedIn = true;
            state.loading = false;
            state.currentUser = Object.assign({}, payload.user, {token: payload.access_token});

            localStorage.setItem("user", JSON.stringify(state.currentUser));
            localStorage.setItem("token", payload.access_token);
        },
        loginFailed(state, payload) {
            state.loading = false;
            state.auth_error = payload.error;
        },
        logout(state) {
            localStorage.removeItem("user");
            localStorage.removeItem("token");
            state.isLoggedIn = false;
            state.currentUser = null;
        },
        updatePosts(state, payload) {
            state.posts = payload;
        },
        CREATE_POST(state, payload) {
            state.posts.push(payload);
        },
        DELETE_POST(state, value) {
            state.posts = state.posts.filter(function (el, index) {
                return value.id !== parseInt(el.id);
            });
        },
        SET_POST_ID(state, id) {
            state.postId = id;
        },
        LOAD_COMMENTS(state, payload) {
            state.comments = payload;
        },
        ADD_COMMENT(state, payload) {
            state.comments.push(payload);
        },
    },
    actions: {
        login(context) {
            context.commit("login");
        },
        getRemoteUser() {
            const userToken = localStorage.getItem("token");

            return new Promise((res, rej) => {
                axios.post('/api/v1/auth/me', {'token': userToken})
                    .then((response) => {
                        console.log(test);
                    })
                    .catch((err) =>{
                        rej("Wrong email or password");
                    })
            });
        },
        getPosts(context) {
            axios.get('/api/v1/posts')
                .then((response) => {
                    context.commit('updatePosts', response.data.data);
                })
        },
        setPostId({commit}, id) {
            commit('SET_POST_ID', id);
        },
        createPost({commit}, data) {
            axios.post('/api/v1/create/post', data.post)
                .then(function (response) {
                    if (response.data.data !== null) {
                        console.log('Add new post');
                        commit('CREATE_POST', response.data.data);
                    }
                })
                .catch(response => console.log(response));
        },
        deletePost({commit}, id) {
            axios.post('/api/v1/posts/delete', {'post_id': id})
                .then(function (response) {
                    if (response.status === 200) {
                        console.log('Remove post');
                        commit('DELETE_POST', id);
                    }
                })
                .catch(response => console.log(response));
        },
        loadComments({commit}, id) {
            axios.get('/api/v1/posts/'+id+'/comments')
                .then((response) => {
                    commit('LOAD_COMMENTS', response.data.data);
                });
        },
        addComment({commit}, comment) {

            axios.post('/api/v1/comments', {
                text: comment.text,
                user_id: comment.author_id,
                parent_id: comment.parent_id,
                post_id: this.state.postId,
            })
                .then(function (response) {
                    if (response.data.data !== null) {
                        commit('ADD_COMMENT', response.data.data);
                        console.log('Add new Comment');
                    }
                })
                .catch(response => console.log(response));
        },
    }
};
