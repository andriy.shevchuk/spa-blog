export function initialize(store, router) {
    router.beforeEach((to, from, next) => {
        const requiresAuth = to.matched.some(record => record.meta.requiresAuth);
        const currentUser = store.state.currentUser;

        if(requiresAuth) {
            axios.get('/api/v1/me', {
                headers: {'Authorization': 'Bearer ' + localStorage.getItem("token")}
            })
                .then((response) => {
                    if (response.data.id !== currentUser.id) {
                        store.commit('logout');
                        router.push('/login');                    }
                })
                .catch((error) => {
                    if (error.response.status === 401) {
                        store.commit('logout');
                        router.push('/login');                    }
                });
        }

        if (requiresAuth && !currentUser) {
            next('/login');
        } else if (to.path === '/login' && currentUser) {
            next('/');
        } else {
            next();
        }
    });

    axios.interceptors.response.use(null, (error) => {
        if (error.response.status === 401) {
            store.commit('logout');
            router.push('/login');
        }

        return Promise.reject(error);
    });

    if (store.getters.currentUser) {
        setAuthorization(store.getters.currentUser.token);
    }
}

export function setAuthorization(token) {
    axios.defaults.headers.common["Authorization"] = `Bearer ${token}`
}
