<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::prefix('v1')->group(function () {

    Route::group([ 'prefix' => 'auth' ], function ($router) {

        Route::post('register', 'Api\v1\AuthController@register');
        Route::post('login', 'Api\v1\AuthController@login');
        Route::post('logout', 'Api\v1\AuthController@logout');
        Route::post('refresh', 'Api\v1\AuthController@refresh');

    });

    Route::get('posts', 'Api\v1\PostController@index');
    Route::get('posts/{id}', 'Api\v1\PostController@get');
    Route::get('posts/{id}/comments', 'Api\v1\PostCommentController@index');

    Route::group(['middleware' => 'jwt.auth'], function ($router) {

        Route::get('me', 'Api\v1\AuthController@me');
        Route::post('create/post', 'Api\v1\PostController@store');
        Route::post('posts/delete', 'Api\v1\PostController@destroy');
        Route::post('comments', 'Api\v1\PostCommentController@store');
        Route::post('formSubmit','Api\v1\MediaController@upload');


    });
});


